# How to contribute to the recalbox project

1. Create an **Issue** on the Recalbox main repository [recalbox/recalbox](https://gitlab.com/recalbox/recalbox/-/issues).
2. Create corresponding **Merge Requests** on Recalbox projects ([recalbox/recalbox](https://gitlab.com/recalbox/recalbox), [recalbox/recalbox-emulationstation](https://gitlab.com/recalbox/recalbox-emulationstation), [recalbox/recalbox-configgen](https://gitlab.com/recalbox/recalbox-configgen), ...).
3. Link your **Merge Requests** to your issue, simply adding the link of the issue in each merge request description.
4. Setting the ~"Testing::Beta" label **to your issue** will automatically embed all your related Merge Requests on the next beta.