#!/bin/bash

interface=wlan0
config_file=/recalbox/share/system/recalbox.conf
config_file_boot=/boot/recalbox-backup.conf
wpa_file=/etc/wpa_supplicant.conf
INIT_SCRIPT=$(basename "$0")
system_setting="recalbox_settings"

# if /recalbox/share is not yet mounted
if ! [ -f "$config_file" ]; then
  # use the boot version of the file
  config_file="$config_file_boot"
fi

mask2cidr() {
    nbits=0
    IFS=.
    for dec in $1 ; do
        case $dec in
            255) let nbits+=8;;
            254) let nbits+=7;;
            252) let nbits+=6;;
            248) let nbits+=5;;
            240) let nbits+=4;;
            224) let nbits+=3;;
            192) let nbits+=2;;
            128) let nbits+=1;;
            0);;
            *) echo "Error: $dec is not recognised"; exit 1
        esac
    done
    echo "$nbits"
}

# Function to create wifi profiles based on user settings
rb_wifi_configure() {
  [ "$1" = "1" ] && X="" || X="$1"
  settings_ssid=$("$system_setting" -command load -key "wifi${X}.ssid" -source "$config_file")
  settings_key=$("$system_setting" -command load -key "wifi${X}.key" -source "$config_file")
  settings_ip=$("$system_setting" -command load -key "wifi${X}.ip" -source "$config_file")
  settings_gateway=$("$system_setting" -command load -key "wifi${X}.gateway" -source "$config_file")
  settings_netmask=$("$system_setting" -command load -key "wifi${X}.netmask" -source "$config_file")
  settings_nameservers=$("$system_setting" -command load -key "wifi${X}.nameservers" -source "$config_file")

  # setup wpa_supplicant network
  if [[ "$settings_ssid" != "" ]] ;then

    recallog -s "${INIT_SCRIPT}" -t "WIFI" "Configuring wifi for SSID: $settings_ssid"
    network=`wpa_cli -i ${interface} add_network`
    wpa_cli -i "$interface" set_network "$network" ssid "\"$settings_ssid\"" || exit 1
    if [ -n "$settings_key" ]; then
        # Connect to protected wifi
        wpa_cli -i "$interface" set_network "$network" psk "\"$settings_key\"" || exit 1
        wpa_cli -i "$interface" set_network "$network" key_mgmt WPA-PSK WPA-EAP WPA-PSK-SHA256 NONE SAE || exit 1
        wpa_cli -i "$interface" set_network "$network" sae_password "\"$settings_key\"" || exit 1
        wpa_cli -i "$interface" set_network "$network" ieee80211w 1 || exit 1
    else
        # Connect to open ssid
        wpa_cli -i "$interface" set_network "$network" key_mgmt NONE || exit 1
    fi
    wpa_cli -i "$interface" set_network "$network" scan_ssid 1 || exit 1
    wpa_cli -i "$interface" enable_network "$network" || exit 1

    # static ip configuration in dhcpcd.conf
    sed -i "/\b\($interface\|static\)\b/d" /etc/dhcpcd.conf
    if [[ "$settings_ip" != "" ]] && \
      [[ "$settings_gateway" != "" ]] && \
      [[ "$settings_netmask" != "" ]] && \
      [[ "$settings_nameservers" != "" ]]; then
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "static ip configuration"
      settings_netmask=$(mask2cidr $settings_netmask)
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "static ip_address=$settings_ip/$settings_netmask"
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "static routers=$settings_gateway"
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "static domain_name_servers=$settings_nameservers"
      echo "interface $interface" >> /etc/dhcpcd.conf
      echo "static ip_address=$settings_ip/$settings_netmask" >> /etc/dhcpcd.conf
      echo "static routers=$settings_gateway" >> /etc/dhcpcd.conf
      echo "static domain_name_servers=$settings_nameservers" >> /etc/dhcpcd.conf
    fi

  fi

}

# function to drop all of the wpa_suppliment networks
wpa_drop_all_networks() {
  # limit any infinite loop to 10 max
  for i in {1..10}; do

    # get the last network id
    netid=`wpa_cli -i $interface list_networks | tail -n1` 
    netid=$(echo $netid | tr " " "\n")

    # exit at the header record
    if [[ $netid == network* ]]; then
      break
    fi

    # remove network id
    wpa_cli -i "$interface" remove_network "$netid" || exit 1
  done
}

enable_wifi() {
  # enable interface
  ifconfig "$interface" up

  # start wpa_supplicant
  pid=$(pgrep wpa_supplicant)
  while kill -0 $pid 2> /dev/null; do sleep 0.2; done; # Prevent race conditions
  wpa_supplicant -B -i "$interface" -c "$wpa_file" -Dnl80211,wext -f /var/log/wpa_supplicant.log


  # clear wpa_supplicant configuration and rebuild
  wpa_drop_all_networks
  wpa_cli -i "$interface" set update_config 1

  settings_region=$("$system_setting" -command load -key "wifi.region" -source "$config_file")
  wpa_cli -i "$interface" set country "$settings_region"

  # iterate through all network ...
  for i in {1..3}; do
    rb_wifi_configure $i
  done

  # write wpa_supplicant configuration
  wpa_cli -i "$interface" save_config
}

start() {
  # turn on rf signals
  if [ -c /dev/rfkill ]; then
    rfkill unblock all
  fi

  if [ -f "$config_file" ]; then
    if [ "$("$system_setting" -command load -key "wifi.enabled" -source "$config_file")" -ne 0 ]; then
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "Wifi option enabled, trying to configure"
      #sleep 2 # wait a bit to be sure tmpfs is initialized
      mount -o remount,rw /
      enable_wifi
      mount -o remount,ro /
    else
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "Wifi option disabled"
      stop
    fi
  else
    recallog -s "${INIT_SCRIPT}" -t "WIFI" "${config_file} not found"
  fi
}

stop() {
  # stop wpa_supplicant
  killall -q wpa_supplicant

  # disable interface
  ifconfig "$interface" down
}

# Main
case "$1" in
  start)
    start
	;;
  stop)
    stop
	;;
  restart|reload)
    stop
    start
	;;
  *)
    echo "Usage $0 {start|stop|restart}"
    exit 1
esac

exit $?
