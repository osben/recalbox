#!/bin/bash

declare -A ES_CONFIG=( \
    ["emulationstation.theme.recalbox-next.systemview"]="9-240p" \
    ["emulationstation.theme.recalbox-next.gamelistview"]="10-240p" \
    ["emulationstation.theme.recalbox-next.menuset"]="7-240p" \
    ["emulationstation.theme.recalbox-next.gameclipview"]="3-240p" \
    ["emulationstation.screensaver.type"]="demo" \
    ["240ptestsuite.ignore"]="0" \
    ["global.smooth"]="0" \
    )

declare -A ES_CLEAN_CONFIG=( \
    ["global.smooth"]="1" \
    ["240ptestsuite.ignore"]="1" \
    )

RECALBOX_CONF="/recalbox/share/system/recalbox.conf"
CRT_OPTIONS_FILE="/boot/crt/recalbox-crt-options.cfg"
CRT_DAC_FILE="/boot/crt/recalbox-crt-config.txt"
DAC_DIRECTORY="/boot/crt/dacs"

function configure_es {
    recallog -s "S13crt" -t "CRT" "Adding configuration in recalbox.conf"
    echo "" >> "${RECALBOX_CONF}"
    for KEY in "${!ES_CONFIG[@]}"; do
        if grep -q -e "^.\?${KEY}=.*" "${RECALBOX_CONF}"; then
            sed -i "s/^.\?${KEY}=.*/${KEY}=${ES_CONFIG[$KEY]}/g" \
                "${RECALBOX_CONF}"
        else
            echo "${KEY}=${ES_CONFIG[$KEY]}" >> "${RECALBOX_CONF}"
        fi
    done
}

function unconfigure {
    recallog -s "S13crt" -t "CRT" "Removing retroarch-custom.cfg and retroarch-core-options.cfg"
    rm -rf "/recalbox/share/system/configs/retroarch/retroarchcustom.cfg" "/recalbox/share/system/configs/retroarch/cores/retroarch-core-options.cfg"
    recallog -s "S13crt" -t "CRT" "Removing configuration in recalbox.conf"
    for KEY in "${!ES_CONFIG[@]}"; do
        sed -i "/${KEY}=${ES_CONFIG[$KEY]}/d" \
            "${RECALBOX_CONF}"
    done
    
    echo "" >> "${RECALBOX_CONF}"
    for KEY in "${!ES_CLEAN_CONFIG[@]}"; do
        if grep -q -e "${KEY}=.*" "${RECALBOX_CONF}"; then
            sed -i "s/${KEY}=.*/${KEY}=${ES_CLEAN_CONFIG[$KEY]}/g" \
            "${RECALBOX_CONF}"
        else
            echo "${KEY}=${ES_CLEAN_CONFIG[$KEY]}" >> "${RECALBOX_CONF}"
        fi
    done
}

function migrate {
    CONFIG_LINE=$(grep -m 1 -e "^system\.crt=.*" "${RECALBOX_CONF}")
    DAC=${CONFIG_LINE##*=}
    if [ "${DAC}" != "" ];then
        mount -o remount,rw /boot
        echo "adapter.type = ${DAC}" > ${CRT_OPTIONS_FILE}
        mount -o remount,ro /boot
        sed -i "/system\.crt=.*/d" "${RECALBOX_CONF}"
    fi
}

if test "$1" == "start" ; then
    RRGBDUAL_FILE="/sys/firmware/devicetree/base/hat/product"
    RRGBDUAL_LASTBOOT="/boot/crt/.stamprrgbdual"

    migrate

    # RRGBD installation
    if grep -q "Recalbox RGB Dual" "${RRGBDUAL_FILE}" && [ ! -f "${RRGBDUAL_LASTBOOT}" ]; then
        recallog -s "S13crt" -t "CRT" "Processing Recalbox RGB Dual automatic installation."
        configure_es
        mount -o remount,rw /boot
        touch "${RRGBDUAL_LASTBOOT}"
        mount -o remount,ro /boot
        exit 0
    fi

    # RRGBD uninstallation
    if ! grep -q "Recalbox RGB Dual" "${RRGBDUAL_FILE}" && [ -f "${RRGBDUAL_LASTBOOT}" ]; then
        recallog -s "S13crt" -t "CRT" "Processing Recalbox RGB Dual automatic uninstallation."
        unconfigure
        mount -o remount,rw /boot
        rm "${RRGBDUAL_LASTBOOT}"
        mount -o remount,ro /boot
        exit 0
    fi

    # Other dacs
    CONFIG_LINE=$(grep -m 1 -e "^adapter\.type = .*" "${CRT_OPTIONS_FILE}")
    DAC=${CONFIG_LINE##*"= "}

    if [[ "${DAC}" == "recalboxrgbdual" || "${DAC}" == "vga666" || "${DAC}" == "rgbpi" || "${DAC}" == "pi2scart" ]];then
        if [[ ! -f "${CRT_DAC_FILE}" ]] || ! grep -q "#device=${DAC}" "${CRT_DAC_FILE}"; then
            source /recalbox/scripts/recalbox-utils.sh
            recallog -s "S13crt" -t "CRT" "Processing ${DAC} configuration."
            mount -o remount,rw /boot

            recallog -s "S13crt" -t "CRT" "Installing config in /boot/crt/"
            cp "${DAC_DIRECTORY}/${DAC}-config.txt" "${CRT_DAC_FILE}"
            mount -o remount,ro /boot
            configure_es
            reboot
        fi
    elif [[ "${DAC}" == "" ]]; then
        if [[ -f "${CRT_DAC_FILE}" ]] && grep -q "#device=" "${CRT_DAC_FILE}"; then
            # We should clean
            source /recalbox/scripts/recalbox-utils.sh
            recallog -s "S13crt" -t "CRT" "Uninstalling CRT configuration."
            mount -o remount,rw /boot
            rm "${CRT_DAC_FILE}"
            touch "${CRT_DAC_FILE}"
           
            unconfigure

            mount -o remount,ro /boot
            reboot
        fi
    else
        # Unsupported
        source /recalbox/scripts/recalbox-utils.sh
        recallog -s "S13crt" -t "CRT" "Unable to process ${DAC} configuration. Not supported yet."
    fi
fi